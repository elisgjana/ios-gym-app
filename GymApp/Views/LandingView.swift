//
//  ContentView.swift
//  GymApp
//
//  Created by Elis Gjana on 26.04.22.
//

import SwiftUI

struct LandingView: View {
    @StateObject private var viewModel = LandingViewModel()
    
    var title: some View {
        Text(viewModel.title)
            .font(.system(size: 64, weight: .medium))
            .foregroundColor(.white)
    }
    
    var createButton: some View {
        Button(action: {
            viewModel.createPushed = true
        }) {
            HStack {
                Image(systemName: viewModel.createButtonImageName)
                    .font(.system(size: 24))
                    .foregroundColor(.white)
                Text(viewModel.createButtonTitle)
                    .font(.system(size: 24))
                    .foregroundColor(.white)
            }
        }
        .padding(.horizontal, 15)
        .buttonStyle(PrimaryButtonStyle())
    }
    
    var alreadyButton: some View {
        Button(viewModel.alreadyButtonTitle) {
            viewModel.loginSigupPushed = true
            
        }.foregroundColor(.white)
    }
    
    var backgroundImage: some View {
        Image(viewModel.backgroundImageName)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .overlay(Color.black.opacity(0.4))
    }
    
    var body: some View {
        NavigationView {
            GeometryReader { proxy in
                VStack {
                    Spacer().frame(height: proxy.size.height * 0.18)
                    title
                    Spacer()
                    NavigationLink(destination: CreateView(), isActive: $viewModel.createPushed) {}
                    createButton
                    
                    NavigationLink(
                        destination: LoginSignupView(
                            viewModel: .init(
                                mode: .login,
                                isPushed: $viewModel.loginSigupPushed
                            )
                        ),
                        isActive: $viewModel.loginSigupPushed
                    ) {
                    }
                    alreadyButton
                }
                .padding(.bottom, 15)
                .frame(
                    maxWidth: .infinity,
                    maxHeight: .infinity
                )
                .background(
                    backgroundImage
                        .frame(width: proxy.size.width)
                        .edgesIgnoringSafeArea(.all)
                )
            }
        }.accentColor(.primary)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LandingView().previewDevice("iPhone 11 Pro")
    }
}
