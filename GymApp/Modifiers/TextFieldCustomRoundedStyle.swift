//
//  TextFieldRoundedStyle.swift
//  GymApp
//
//  Created by admin on 14.05.22.
//

import SwiftUI

struct TextFieldCustomRoundedStyle: ViewModifier {
    func body(content: Content) -> some View {
        return content
            .font(.system(size: 16, weight: .medium))
            .foregroundColor(.primary)
            .padding()
            .cornerRadius(16)
            .overlay(
                RoundedRectangle(cornerRadius: 16)
                    .stroke(
                        Color.primary
                    )
            )
            .padding(.horizontal, 15)
    }
}
