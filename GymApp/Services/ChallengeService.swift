//
//  ChallengeService.swift
//  GymApp
//
//  Created by admin on 01.05.22.
//

import Combine
import FirebaseFirestore
import FirebaseFirestoreSwift

protocol ChallengeServiceProtocol {
    func create(_ challenge: Challenge) -> AnyPublisher<Void, GymError>
    func observeChallenges(userId: UserId) -> AnyPublisher<[Challenge], GymError>
    func delete(_ challengeId: String) -> AnyPublisher<Void, GymError>
}

final class ChallengeService: ChallengeServiceProtocol {
    private let db = Firestore.firestore()
    
    func create(_ challenge: Challenge) -> AnyPublisher<Void, GymError> {
        return Future<Void, GymError> { promise in
            do {
                _ = try self.db.collection("challenges").addDocument(from: challenge) { error in
                    if let error = error {
                        promise(.failure(.default(description: error.localizedDescription)))
                    }else {
                        promise(.success(()))
                    }
                }
            } catch {
                promise(.failure(.default()))
            }
        }.eraseToAnyPublisher()
    }
    
    func observeChallenges(userId: UserId) -> AnyPublisher<[Challenge], GymError> {
        let query = db.collection("challenges").whereField("userId", isEqualTo: userId)
        
        return Publishers.QuerySnapshotPublisher(query: query)
            .flatMap { snapshot -> AnyPublisher<[Challenge], GymError> in
                do {
                    let challenges = try snapshot.documents.compactMap {
                        try $0.data(as: Challenge.self)
                    }
                    return Just(challenges).setFailureType(to: GymError.self).eraseToAnyPublisher()
                } catch {
                    return Fail(error: .default(description: "Parsing error")).eraseToAnyPublisher()
                }
            }
            .eraseToAnyPublisher()
    }
    
    func delete(_ challengeId: String) -> AnyPublisher<Void, GymError> {
        return Future<Void, GymError> { promise in
            self.db.collection("challenges").document(challengeId).delete() { error in
                if let error = error {
                    promise(.failure(.default(description: error.localizedDescription)))
                } else {
                    promise(.success(()))
                }
            }
        }.eraseToAnyPublisher()
    }
}
