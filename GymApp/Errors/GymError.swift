//
//  GymError.swift
//  GymApp
//
//  Created by admin on 01.05.22.
//

import Foundation

enum GymError: LocalizedError {
case auth(description: String)
case `default`(description: String? = nil)
    
    var errorDescription: String {
        switch self {
            
        case .auth(description: let description):
            return description
        case .default(description: let description):
            return description ?? "Something went wrong"
        }
    }
}
