//
//  ProgressCircleViewModel.swift
//  GymApp
//
//  Created by admin on 14.05.22.
//

import Foundation

struct ProgressCircleViewModel {
    let title: String
    let message: String
    let percentageComplete: Double
    var shouldShowTitle: Bool {
        percentageComplete <= 1 
    }
}
