//
//  SettingsItemViewModel.swift
//  GymApp
//
//  Created by admin on 12.05.22.
//

import Foundation

extension SettingsViewModel {
    struct SettingsItemViewModel {
        let title: String
        let name: String
        let type: SettingsItemType
        
        enum SettingsItemType {
            case account
            case mode
            case privacy
            case logout
        }
    }
}

