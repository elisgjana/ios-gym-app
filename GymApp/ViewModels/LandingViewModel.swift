//
//  LandingViewModel.swift
//  GymApp
//
//  Created by admin on 14.05.22.
//

import Foundation

final class LandingViewModel: ObservableObject {
    @Published var loginSigupPushed = false
    @Published var createPushed = false
    
    let title = "Increment"
    let createButtonTitle = "Create a challenge"
    let createButtonImageName = "plus.circle"
    let alreadyButtonTitle = "I already have an account"
    let backgroundImageName = "background"
}
