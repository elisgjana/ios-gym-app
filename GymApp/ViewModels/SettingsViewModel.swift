//
//  SettingsViewModel.swift
//  GymApp
//
//  Created by admin on 12.05.22.
//

import Combine
import SwiftUI


final class SettingsViewModel: ObservableObject {
    @AppStorage("isDarkMode") private var isDarkMode = false
    @Published private(set) var itemViewModels: [SettingsItemViewModel] = []
    @Published var loginSignupPushed = false
    private let userService: UserServiceProtocol
    private var cancellables: [AnyCancellable] = []
    
    init(userService: UserServiceProtocol = UserService()) {
        self.userService = userService
    }
    
    let title = "Settings"
    
    func item(at index: Int) -> SettingsItemViewModel {
        itemViewModels[index]
    }
    
    func tappedItem(at index: Int) {
        switch itemViewModels[index].type {
        case .account:
            guard userService.currentUser?.email == nil else { return }
             loginSignupPushed = true
            
        case .mode:
            isDarkMode = !isDarkMode
            buildItems()
            
        case .privacy:
            print("")
            
        case .logout:
            userService.logout().sink { completion in
                switch completion {
                case let .failure(error):
                    print(error.localizedDescription)
                case .finished: break
                }
            } receiveValue: { _ in
            }.store(in: &cancellables)
        }
    }
    
    private func buildItems() {
        itemViewModels = [
            .init(title: userService.currentUser?.email ?? "Create Account", name: "person.circle", type: .account),
            .init(title: "Switch to \(isDarkMode ? "Light" : "Dark") Mode", name: "lightbulb", type: .mode),
            .init(title: "Privacy Policy", name: "shield", type: .privacy)
        ]
        if userService.currentUser?.email != nil {
            itemViewModels += [.init(title: "Logout", name: "arrowshape.turn.up.left", type: .logout)]
        }
    }
    
    func onAppear() {
        buildItems()
    }
}
